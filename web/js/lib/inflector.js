if (!String.prototype.underscore) {
    Object.defineProperty(String.prototype, 'underscore', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function () {
            return this.replace(/::/g, '/')
                .replace(/([A-Z]+)([A-Z][a-z])/g, '$1_$2')
                .replace(/([a-z\d])([A-Z])/g, '$1_$2')
                .replace(/-/g, '_')
                .toLowerCase();
        }
    });
}

if (!String.prototype.dasherize) {
    Object.defineProperty(String.prototype, 'dasherize', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function () {
            return this.replace(/_/g, '-');
        }
    });
}

var Inflector = function() {
    this.cache = {
        underscore: {},
        latinalize: {},
        slugalize: {}
    };

    this.cyrillic_letters = ['А','Б','В','Г','Д','Е','Ж','З','И','Й','К','Л','М','Н','О',
        'П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ь','Ю','Я',
        'а','б','в','г','д','е','ж','з','и','й','к','л','м','н','о',
        'п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ь','ю','я'];

    this.latin_letters = ['A','B','V','G','D','E','Zh','Z','I','J','K','L','M','N','O',
        'P','R','S','T','U','F','H','C','Ch','Sh','Sht','Y','J','Ju','Q',
        'a','b','v','g','d','e','zh','z','i','j','k','l','m','n','o',
        'p','r','s','t','u','f','h','c','ch','sh','sht','y','j','ju','q'];
};

Inflector.prototype = {
    underscore: function(word) {
        if(this.cache['underscore'][word])
            return this.cache['underscore'][word];

        return this.cache['underscore'][word] = word.underscore().dasherize();
    },

    latinalize: function(word) {
        if(this.cache['latinalize'][word])
            return this.cache['latinalize'][word];

        var self = this;
        var result = word.split('').map(function(letter) {
            var cc = letter.charCodeAt(0);
            if(cc >= 1040 && cc <= 1103) {
                letter = self.latin_letters[self.cyrillic_letters.indexOf(letter)];
            }

            return letter;
        }).join('');

        return this.cache['latinalize'][word] = result;
    },

    slugalize: function(word) {
        if(this.cache['slugalize'][word])
            return this.cache['slugalize'][word];

        var result = this.latinalize(word);
        result = result.replace(/(\s+|[^0-9^A-Z^a-z])/g, '-');
        result = this.underscore(result);
        result = result.replace(/[-_]+/g, '-').replace(/(^-+|-+$)/, '');
        return this.cache['slugalize'][word] = result;
    }
};