;(function ( $, window, document, undefined ) {

    "use strict";
    var pluginName = "simpleFileBrowser";


    //Plugin constructor
    function Plugin ( element, options ) {

        this.element = element;
        this.$element = $(element);
        this.name = pluginName;

        // Extend default options with those supplied by user.
        this.opts = $.extend(true, {}, $.fn[pluginName].defaults, options);

        this.$elements = {
            breadcrumbs:null,
            content:null,
        }

        var selected=null;
        var path=[];

        this.init(element,options);
    };


    //Avoid Plugin.prototype conflicts
    Plugin.prototype =  {


        //Get files
        _get: function() {

            var me=this;

            if (me.opts.json) {

                var dfd = jQuery.Deferred();

                dfd.resolve(me.opts.json[me._pathToString()]);

                return dfd.promise();

            } else {

                var data=me.opts.data;
                data.path=me._pathToString();

                return $.ajax({
                    url: me.opts.url,
                    dataType: "json",
                    type: me.opts.method,
                    async: true,
                    cache: false,
                    data: data
                });
            }

        },


        _icons: function(type) {

            var icon='';

            switch (type) {
                case "folder":
                    icon = "glyphicon glyphicon-folder-close";
                    break;
                case "ogg":
                case "wav":
                case "mp3":
                    icon = "glyphicon glyphicon-volume-up";
                    break;
                case "docx":
                case "doc":
                case "odt":
                case "xls":
                case "pps":
                    icon = "glyphicon glyphicon-folder-close";
                    break;
                case "tiff":
                case "gif":
                case "bmp":
                case "png":
                case "jpeg":
                case "jpg":
                    icon = "glyphicon glyphicon-picture";
                    break;
                case "gz":
                case "tar":
                case "rar":
                case "zip":
                    icon = "glyphicon glyphicon-book";
                    break;
                case "mpg":
                case "avi":
                case "mp4":
                case "flv":
                    icon = "glyphicon glyphicon-film";
                    break;
                case "pdf":
                    icon = "glyphicon glyphicon-folder-close";
                    break;
                case "txt":
                case "sql":
                case "php":
                case "css":
                case "js":
                case "config":
                    icon = "glyphicon glyphicon-cog";
                    break;
                default:
                    icon = "glyphicon glyphicon-file"
            }

            return icon;
        },


        //Trim implementation
        _trim: function(string, char, direction) {

            if (char === undefined)
                char = "\s";

            if (direction === undefined)
                direction = "both";

            if(direction=='left' || direction=='both') {
                string = string.replace(new RegExp("^[" + char + "]+"), "");
            }

            if(direction=='right' || direction=='both') {
                string = string.replace(new RegExp("[" + char + "]+$"), "");
            }

            return string;
        },


        //Modify path
        _pathChange: function(name) {

            var me=this;

            if (name!=undefined) {
                if (name == '..') {
                    me.path.pop()
                } else {
                    me.path.push(name)
                }
            }

        },


        //Return string representation path
        _pathToString: function(num) {

            var me=this;

            if (num==undefined) var array=me.path;
            else var array=me.path.slice(0,num);

            var string=array.length==1 ? '/' : array.join("/");

            return string

        },


        //Draw folders and files
        _draw: function(json, path) {
            if (path.charAt(0) !== '/') {
                path = '/' + path;
            }

            var me=this;

            me.$elements.content.html('');

            me.$element.removeClass('x32 x22 x16');
            me.$element.addClass('x'+me.opts.size);

            me.$elements.content.removeClass('icon details');
            me.$elements.content.addClass(me.opts.view);

            //Breadcrumbs
            if (me.opts.breadcrumbs) {

                me.$elements.breadcrumbs.html('');

                $.each(me.path, function (key, value) {

                    var nom = (value=='' ? 'root' : value);
                    var $span = $("<li />").data('path', me._pathToString(key+1)).text(nom);
                    me.$elements.breadcrumbs.append($span)

                });

            }

            var html='<ul>';
            $.each(json[path],function(key,value){
                html+='<li data-name="'+value.name+'" data-type="'+(value.type=='folder'?'folder':'file')+'">' +
                    '<i class="'+(me._icons(value.type))+'"></i>' +
                    '<span>'+(value.name||'')+'</span>'+
                    '</li>';
            });
            html+='</ul>';

            me.$elements.content.html(html);

        },


        //Initialize plugin
        init: function () {

            var me=this;

            //Path
            me.path=me._trim(this.opts.path,'/','right').split('/');


            //Clean and add elements
            me.$element.html('');
            me.$element.addClass('sfb');

            if (me.opts.breadcrumbs==true) {
                me.$elements.breadcrumbs = $("<ul />").addClass("sfbBreadCrumbs");
                me.$element.append(me.$elements.breadcrumbs);

                me.$elements.breadcrumbs.on("click","li",function() {
                    var $li=$(this);
                    me.path=me._trim($li.data("path"),'/','right').split('/');
                    me._get().then(function(json) {
                        me._draw(json, me._trim($li.data("path"),'/','right'));
                    });
                });
            }

            me.$elements.content=$("<div />").addClass("sfbContent");
            me.$element.append(me.$elements.content);


            //Watchers
            var open = function($li) {

                if ($li.data("type")=='folder') {
                    me._pathChange($li.data("name"));

                    me._get().then(function(json) {
                        me._draw(json, $li.data("name"));
                    });
                }

                me.opts.onOpen.call(me, me, $li.data("name"), me._pathToString(), $li.data("type"));
            };

            var select = function($li) {

                if ($li.data("name")!='..') {
                    me.$elements.content.find("li").removeClass("selected");
                    $li.addClass("selected");

                    me.selected = me._pathToString() + $li.data("name");
                    me.opts.onSelect.call(me, me, $li.data("name"), me._pathToString(), $li.data("type"));
                }

            };

            var clicks=0;
            var timer=1
            var clicksOpen= me.opts.select ? 2 : 1;

            me.$elements.content.on("click","li",function() {

                var $li=$(this);

                clicks++;

                if(clicks === 1) {

                    timer = setTimeout(function() {

                        if (me.opts.select) select($li);
                        else open($li);

                        clicks = 0;

                    }, 200);

                } else {

                    clearTimeout(timer);

                    if (me.opts.select) select($li);
                    open($li);

                    clicks = 0;
                }

            });


            //Initial content
            me._get().then(function(json){
                me._draw(json, me.opts.path);
                me.opts.onLoad.call(me,me);
            });

        },


        //Destroy plugin
        remove: function() {

            //Remove elements
            this.html('');

            //Remove data
            this.$element.removeData('plugin_' + pluginName);
            this.$element.removeClass('sfb x32 x22 x16');

        },


        //Get selected file
        getSelected: function() {
            var me=this;

            return me.selected;
        },

        //Get current location
        getPath: function() {
            var me=this;

            return me._pathToString();
        },


        //Redraw content
        redraw: function(path) {
            var me=this;

            me._get().then(function(json) {
                me._draw(json, path);
            })
        },


        //Change options
        chgOption: function(opts) {
            var me=this;

            if (typeof opts.view !== 'undefined') me.opts.view=opts.view;
            if (typeof opts.size !== 'undefined') me.opts.size=opts.size;
            if (typeof opts.select !== 'undefined') me.opts.select=opts.select;
            if (typeof opts.path !== 'undefined') {
                me.path=me._trim(opts.path,'/','right').split('/');
            }

            me.redraw(me._trim(opts.path,'/','right'));

        }

    };


    //Plugin definition
    $.fn[pluginName] = function(options) {

        //Public method
        if (typeof arguments[0] === 'string') {

            var args = Array.prototype.slice.call(arguments, 1);

            if ($.isFunction(Plugin.prototype[options])) {
                var response;
                this.each(function() {
                    response= $.data(this, 'plugin_' + pluginName)[options](args[0]);
                });

                //If exists method response, return response
                if (typeof response !== 'undefined') return response;
                else return this;
            } else {
                $.error('Method ' + options + ' is not available');
            }

            //New instance of the plugin
        } else if (typeof options === "object" || !options) {
            return this.each(function() {
                if (!$.data(this, 'plugin_' + pluginName)) {
                    $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
                }
            });
        }

    };


    //Default options
    $.fn[pluginName].defaults = {
        url: 'folder.php',
        json: null,
        method: 'post',
        view: "details", //icon, details
        size: "32",
        path: "/",
        breadcrumbs: false,
        select: true,
        data: {},
        onSelect:function() {},
        onOpen:function() {},
        onDestroy:function() {},
        onLoad:function() {}
    };


})( jQuery, window, document );