requirejs.config({
    baseUrl: document.getElementsByTagName('body')[0].getAttribute('data-web-path'),
    shim : {
        "bootstrap" : ['jquery']
    },
    paths: {
        "jquery"                : "lib/jquery-2.2.4.min",
        "bootstrap"             : "lib/bootstrap-3.3.7.min",
        "bootstrap-datepicker"  : "lib/bootstrap-datepicker.min",
        "ckeditorCore"          : "lib/ckeditor/ckeditor",
        "inflector"             : "lib/inflector",
        "filebrowser"           : "lib/filemanager"
    }
});

require(['modules/frontend'], function(Application) {
    Application.init();
});