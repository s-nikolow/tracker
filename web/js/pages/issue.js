require(
    [
        'modules/richtext',
        'modules/attachment',
        'modules/filemanager'
    ],
    function(Richtext, Attachment, FileManager) {
        Richtext.init('textarea[data-role="ckeditor"]');

        new Attachment({
            addHandler: 'a[data-role="add-collection-item"]',
            removeHandler: 'a[data-role="remove-collection-item"]'
        });

        new FileManager({

        });
    }
);