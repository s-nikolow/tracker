require(
    [
        'modules/richtext',
        'modules/sluggable',
        'bootstrap'
    ],
    function(Richtext, Sluggable) {
        Richtext.init('textarea[data-role="ckeditor"]');
        Sluggable.init('[data-sluggable="true"]');

        var first_error_container = $('form div.has-error:first');
        if (first_error_container.length > 0) {
            var tab_id = first_error_container.parents('.tab-pane').attr('id');

            $('a[href="#'+tab_id+'"]').tab('show');
        }
    }
);