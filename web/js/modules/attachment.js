define(['jquery', 'modules/modal'], function($, Modal) {

    function Attachment(options) {
        options = options || {};

        this.options = $.extend({
            addHandler: null,
            removeHandler: null,
            containerSelector: 'div[data-role="collection"]'
        }, options);

        this.init();
    }

    Attachment.prototype = {
        init: function() {
            if (typeof this.options.addHandler === 'string') {
                $(document).on('click', this.options.addHandler, $.proxy(this.onCollectionItemAppend, this));
            }

            if (typeof this.options.removeHandler === 'string') {
                $(document).on('click', this.options.removeHandler, $.proxy(this.onCollectionItemRemove, this));
            }
        },

        onCollectionItemAppend: function(event) {
            var element = $(event.target);
            var container = element.closest(this.options.containerSelector);
            var items_count = container.find('tbody tr').length;
            var prototype = container.find('div.form-collection').data('prototype');

            prototype = $(prototype.replace(/__name__/g, (items_count + 1)));
            prototype.appendTo(container.find('tbody'));
        },

        onCollectionItemRemove: function(event) {
            var element = $(event.target);
            var row = element.parents('tr');

            if (row.length > 0) {
                new Modal({
                    title: 'Внимание',
                    html_content: '<p>Избраното от вас действие ще изтрие този елемент? Сигурни ли сте, че желаете да продължите?</p>',
                    close_callback: function() {
                        $(this.getOption('id')).remove();
                        $(document).off('click', this.getOption('confirm_button_id'));
                    },
                    confirm_callback: function() {
                        $(this.getOption('id')).modal('hide');
                        row.fadeOut('slow', function() {
                            row.remove();
                        });
                    }
                });
            }
        }
    };

    return Attachment;
});