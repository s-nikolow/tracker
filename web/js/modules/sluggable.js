define(['jquery', 'inflector'], function($) {
    return {
        init: function(handler) {
            var object = $(handler);

            if (object.length > 0) {
                var inflector = new Inflector();

                object.each(function() {
                    var current_element = $(this);
                    var depending_on = current_element.data('depending-on');

                    if (typeof depending_on !== 'undefined') {
                        var dependent_element = $('input[name="'+depending_on+'"]');

                        dependent_element.on('keyup', function() {
                            current_element.val(inflector.slugalize(this.value));
                        });
                    }
                });
            }
        }
    }
});