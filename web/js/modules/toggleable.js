define(['jquery'], function($) {
    return {
        init: function(handler) {
            $(handler).on('click', function(event) {
                event.preventDefault();

                var element = $(this);
                var params = {
                    id: element.data('id'),
                    entity: element.data('entity'),
                    column: element.data('column'),
                    value: element.data('value')
                };

                $.ajax({
                    url: element.data('url'), dataType: 'json', type: 'POST', data: params, success: function(response) {
                        if (typeof response.success !== 'undefined' && response.success) {
                            element.toggleClass(element.data('class-list'));
                            element.data('value', response.value);
                        }
                    }
                });
            });
        }
    }
});