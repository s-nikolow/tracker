define(['jquery', 'modules/toggleable', 'modules/datepicker', 'bootstrap'], function($, Toggleable, Datepicker) {
    return {
        init: function() {
            Toggleable.init('a[data-role="togglable"]');
            Datepicker.init('input[data-role="bs-datepicker"]');
        }
    }
});