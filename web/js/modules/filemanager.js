define(['jquery', 'modules/modal', 'bootstrap', 'filebrowser'], function($, Modal) {

    function FileManager(options) {
        options = options || {};

        this.options = $.extend({
            handler: 'button[data-action="toggle-file-manager"]',
            container: 'div[data-role="fm-container"]'
        }, options);

        this.init();
    }

    FileManager.prototype = {
        init: function() {
            if (typeof this.options.handler === 'string') {
                $(document).on('click', this.options.handler, $.proxy(this.toggle, this));
            }
        },

        toggle: function(event) {
            var element = $(event.target);
            var target = element.parents(this.options.container).find('input[type="text"]');

            var modal_window = new Modal({
                id: '#fm-modal',
                title: 'File Manager',
                dialog_class: 'modal-dialog modal-lg',
                html_content: '<div id="files"></div>',
                has_confirm_button: false,
                has_cancel_button: false,
                show_on_init: false,
                append: true,
                close_callback: function() {
                    $(this.getOption('id')).remove();
                }
            });

            $('#files').simpleFileBrowser({
                url: element.data('url'),
                path: '/',
                view: 'list',
                select: false,
                breadcrumbs: true,
                onOpen: function (obj,file, folder, type) {
                    if (type=='file') {
                        target.attr('value', folder+'/'+file).val(folder+'/'+file);
                        modal_window.close();
                    }
                }
            });

            modal_window.open();
        }
    };

    return FileManager;
});