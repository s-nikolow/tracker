define(['jquery', 'ckeditorCore'], function($) {
    return {
        init: function(selector) {
            CKEDITOR.replace($(selector).attr('id'));
        }
    }
});