define(['jquery', 'bootstrap'], function($) {

    function Modal(options) {
        options = options || {};

        this.object = null;
        this.options = $.extend({
            confirm_callback: null,
            close_callback: null,
            id: '#dataModal',
            header_id: '#dataTitle',
            title: 'Custom modal title',
            confirm_button_id: '#dataOK',
            cancel_button_id: '#dataCancel',
            body_id: '#dataModalBody',
            dialog_class: 'modal-dialog',
            html_content: null,
            has_cancel_button: true,
            has_confirm_button: true,
            show_on_init: true,
            append: false
        }, options);

        this.init();
    }

    Modal.prototype = {
        init: function() {
            this.object = this.buildObjectTemplate();

            if (this.options.has_confirm_button && typeof this.options.confirm_callback === 'function') {
                $(document).on('click', this.options.confirm_button_id, $.proxy(this.options.confirm_callback, this));
            }

            if (typeof this.options.close_callback === 'function') {
                this.object.on('hidden.bs.modal', $.proxy(this.options.close_callback, this));
            }

            if (this.options.append) {
                $('body').append(this.object);
            }

            if (this.options.show_on_init) {
                $(this.object).modal('show');
            }
        },

        buildObjectTemplate: function() {
            var template = ('<div id="{MODAL_ID}" class="modal fade" tabindex="-1" role="dialog"> <div class="{DIALOG_CLASS}" role="document"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 id="{HEADER_ID}" class="modal-title">{MODAL_TITLE}</h4> </div> <div id="{MODAL_BODY_ID}" class="modal-body"></div> <div class="modal-footer">  </div> </div> </div></div>');
            var map = {
                'MODAL_ID': this.options.id.replace('#', ''),
                'HEADER_ID': this.options.header_id.replace('#', ''),
                'MODAL_TITLE': this.options.title.replace('#', ''),
                'MODAL_BODY_ID': this.options.body_id.replace('#', ''),
                'DIALOG_CLASS': this.options.dialog_class
            };

            for (var key in map) {
                template = template.replace(new RegExp('\\{' + key + '\\}', 'gm'), map[key]);
            }

            template = $(template);

            if (this.options.html_content) {
                $(this.options.html_content).appendTo(template.find('.modal-body'));
            }

            if (this.options.has_cancel_button) {
                var button = ('<button type="button" class="btn btn-default" data-dismiss="modal" id="{CANCEL_BUTTON_ID}">Close</button>').replace('{CANCEL_BUTTON_ID}', this.options.cancel_button_id.replace('#', ''));

                $(button).appendTo(template.find('.modal-footer'));
            }

            if (this.options.has_confirm_button) {
                var button = ('<button type="button" class="btn btn-primary" id="{CONFIRM_BUTTON_ID}">OK</button>').replace('{CONFIRM_BUTTON_ID}', this.options.confirm_button_id.replace('#', ''));

                $(button).appendTo(template.find('.modal-footer'));
            }

            return template;
        },

        open: function() {
            $(this.object).modal('show');
        },

        close: function() {
            $(this.object).modal('hide');
        },

        getOption: function(key) {
            if (this.options.hasOwnProperty(key)) {
                return this.options[key];
            }

            return null;
        }
    };

    return Modal;

});