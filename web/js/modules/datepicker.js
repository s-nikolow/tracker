define(['jquery', 'bootstrap-datepicker'], function($) {
    return {
        init: function(selector) {
            $(selector).datepicker({
                format: 'yyyy-mm-dd',
                weekStart: 1,
                clearBtn: true,
                daysOfWeekHighlighted: "0,6",
                calendarWeeks: true,
                autoclose: true,
                todayHighlight: true
            });
        }
    }
});