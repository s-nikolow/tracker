<?php

namespace BugTrackerBundle\Repository;

class TrackerRepository extends AbstractRepository
{

    /**
     * @param bool $cache
     * @return array
     */
    public function findCollectionForList($cache = false)
    {
        return $this->createQueryBuilder('tracker')
            ->orderBy('tracker.id', 'DESC')
            ->getQuery()
            ->useQueryCache($cache)
            ->useResultCache($cache)
            ->getArrayResult();
    }

}