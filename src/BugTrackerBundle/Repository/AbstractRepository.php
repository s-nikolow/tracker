<?php

namespace BugTrackerBundle\Repository;

use Doctrine\ORM\EntityRepository;

abstract class AbstractRepository extends EntityRepository
{

    /**
     * @param string $column
     * @param int $value
     * @param int $id
     * @return mixed
     */
    public function setOppositeValue($column, $value, $id)
    {
        return $this->createQueryBuilder('entity')
            ->update()
            ->set(sprintf('entity.%s', $column), ':value')
            ->where('entity.id = :id')
            ->setParameter('value', intval(!$value))
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }

}