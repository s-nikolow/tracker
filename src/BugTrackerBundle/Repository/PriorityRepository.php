<?php

namespace BugTrackerBundle\Repository;

class PriorityRepository extends AbstractRepository
{

    /**
     * @param bool $cache
     * @return array
     */
    public function findCollectionForList($cache = false)
    {
        return $this->createQueryBuilder('priority')
            ->orderBy('priority.id', 'DESC')
            ->getQuery()
            ->useQueryCache($cache)
            ->useResultCache($cache)
            ->getArrayResult();
    }

}