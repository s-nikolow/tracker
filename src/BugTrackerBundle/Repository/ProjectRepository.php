<?php

namespace BugTrackerBundle\Repository;

use BugTrackerBundle\Entity\Project;
use BugTrackerBundle\Entity\User;
use BugTrackerBundle\Enum\StatusEnum;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query;

class ProjectRepository extends AbstractRepository
{

    /**
     * @param bool $cache
     * @return array
     */
    public function findCollectionForList($cache = false)
    {
        return $this->createQueryBuilder('project')
            ->orderBy('project.id', 'DESC')
            ->getQuery()
            ->useQueryCache($cache)
            ->useResultCache($cache)
            ->getArrayResult();
    }

    /**
     * @param User $user
     * @param bool $cache
     * @return array
     */
    public function findMemberProjects(User $user, $cache = false)
    {
        $builder = $this->createQueryBuilder('project');

        return $builder
            ->innerJoin('project.members', 'project_member')
            ->where($builder->expr()->eq('project_member.id', ':user_id'))
            ->andWhere($builder->expr()->eq('project.is_active', StatusEnum::STATUS_ACTIVE))
            ->setParameter('user_id', $user->getId())
            ->orderBy('project.title', 'ASC')
            ->getQuery()
            ->useQueryCache($cache)
            ->useResultCache($cache)
            ->getArrayResult();
    }

    /**
     * @param string $slug
     * @param User $user
     * @param bool $cache
     * @return mixed
     */
    public function findProject($slug, User $user, $cache = false)
    {
        $builder = $this->createQueryBuilder('project');

        return $builder
            ->innerJoin('project.members', 'project_member')
            ->where($builder->expr()->eq('project_member.id', ':user_id'))
            ->andWhere($builder->expr()->eq('project.slug', ':slug'))
            ->andWhere($builder->expr()->eq('project.is_active', ':is_active'))
            ->setParameters([
                'user_id' => $user->getId(),
                'slug' => filter_var($slug, FILTER_SANITIZE_STRING),
                'is_active' => StatusEnum::STATUS_ACTIVE
            ])
            ->groupBy('project.id')
            ->getQuery()
            ->useQueryCache($cache)
            ->useResultCache($cache)
            ->getOneOrNullResult();
    }

    /**
     * @param Project $project
     * @param bool $cache
     * @return array
     */
    public function findAssignedTrackerItems(Project $project, $cache = false)
    {
        $builder = $this->createQueryBuilder('project');

        return $builder
            ->select('tracker.id, tracker.title')
            ->innerJoin('project.trackers', 'tracker')
            ->where($builder->expr()->eq('project.id', ':project_id'))
            ->setParameter('project_id', $project->getId())
            ->getQuery()
            ->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
//            ->useQueryCache($cache)
//            ->useResultCache($cache)
            ->getResult('KeyPairHydrator');
    }

    /**
     * @param Project $project
     * @param bool $cache
     * @return array
     */
    public function findAssignedPriorityItems(Project $project, $cache = false)
    {
        $builder = $this->createQueryBuilder('project');

        return $builder
            ->select('priority.id', 'priority.title')
            ->innerJoin('project.priorities', 'priority')
            ->where($builder->expr()->eq('project.id', ':project_id'))
            ->setParameter('project_id', $project->getId())
            ->getQuery()
            ->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
//            ->useQueryCache($cache)
//            ->useResultCache($cache)
            ->getResult('KeyPairHydrator');
    }

}