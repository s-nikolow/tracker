<?php

namespace BugTrackerBundle\Repository;

use BugTrackerBundle\Entity\Project;
use Doctrine\ORM\Query;

class IssueRepository extends AbstractRepository
{

    /**
     * @param Project $project
     * @param bool $cache
     * @return array
     */
    public function findCollectionForProject(Project $project, $cache = false)
    {
        $builder = $this->createQueryBuilder('issue');

        return $builder
            ->select('issue', 'status.class_name AS status_class_name', 'priority.class_name AS priority_class_name', 'author.username')
            ->leftJoin('issue.project', 'project')
            ->leftJoin('issue.status', 'status')
            ->leftJoin('issue.priority', 'priority')
            ->leftJoin('issue.created_by', 'author')
            ->where($builder->expr()->eq('project.id', ':project_id'))
            ->setParameter('project_id', $project->getId())
            ->orderBy('issue.created_at', 'DESC')
            ->getQuery()
            ->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
            ->useQueryCache($cache)
            ->useResultCache($cache)
            ->getScalarResult();
    }

    /**
     * @param Project $project
     * @param string $slug
     * @param bool $cache
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findSingleIssue(Project $project, $slug, $cache = false)
    {
        $builder = $this->createQueryBuilder('issue');

        return $builder
            ->select('issue', 'status', 'priority', 'author')
            ->leftJoin('issue.project', 'project')
            ->leftJoin('issue.status', 'status')
            ->leftJoin('issue.priority', 'priority')
            ->leftJoin('issue.created_by', 'author')
            ->where($builder->expr()->eq('project.id', ':project_id'))
            ->andWhere($builder->expr()->eq('issue.slug', ':slug'))
            ->setParameters([
                'project_id' => $project->getId(),
                'slug' => filter_var($slug, FILTER_SANITIZE_STRING)
            ])
            ->getQuery()
            ->useQueryCache($cache)
            ->useResultCache($cache)
            ->getOneOrNullResult();
    }

}