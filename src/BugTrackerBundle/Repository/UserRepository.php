<?php

namespace BugTrackerBundle\Repository;

class UserRepository extends AbstractRepository
{

    /**
     * @param bool $cache
     * @return array
     */
    public function findCollectionForList($cache = false)
    {
        return $this->createQueryBuilder('user')
            ->orderBy('user.id', 'DESC')
            ->getQuery()
            ->useQueryCache($cache)
            ->useResultCache($cache)
            ->getArrayResult();
    }

}