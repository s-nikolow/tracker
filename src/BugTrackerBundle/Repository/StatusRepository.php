<?php

namespace BugTrackerBundle\Repository;

use Doctrine\ORM\Query;

class StatusRepository extends AbstractRepository
{

    /**
     * @param bool $cache
     * @return array
     */
    public function findCollectionForList($cache = false)
    {
        return $this->createQueryBuilder('status')
            ->orderBy('status.id', 'DESC')
            ->getQuery()
            ->useQueryCache($cache)
            ->useResultCache($cache)
            ->getArrayResult();
    }

    /**
     * @return array
     */
    public function findCollectionForProject()
    {
        return $this->createQueryBuilder('status')
            ->select('status.id, status.title')
            ->where('status.is_active = 1')
            ->getQuery()
            ->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
            ->getResult('KeyPairHydrator');
    }

}