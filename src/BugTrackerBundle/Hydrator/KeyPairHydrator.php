<?php

namespace BugTrackerBundle\Hydrator;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;
use PDO;

class KeyPairHydrator extends AbstractHydrator
{

    /**
     * {@inheritdoc}
     * @return array
     */
    protected function hydrateAllData()
    {
        return $this->_stmt->fetchAll(PDO::FETCH_KEY_PAIR);
    }

}