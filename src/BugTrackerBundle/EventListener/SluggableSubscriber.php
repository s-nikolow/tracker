<?php

namespace BugTrackerBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use BugTrackerBundle\Entity\Project;
use BugTrackerBundle\Entity\Issue;
use BugTrackerBundle\Service\Helper\Inflector;

/**
 * Class SluggableSubscriber
 * @package BugTrackerBundle\EventListener
 */
class SluggableSubscriber implements EventSubscriber
{


    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return ['prePersist', 'preUpdate'];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->setSlug($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->setSlug($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    private function setSlug(LifecycleEventArgs $args)
    {
        if ($entity = $args->getEntity()) {
            switch (true) {
                case $entity instanceof Project:
                case $entity instanceof Issue:

                    if (!$entity->getSlug()) {
                        $entity->setSlug(Inflector::slugalize($entity->getTitle()));
                    }

                    break;
            }
        }
    }

}