<?php

namespace BugTrackerBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use BugTrackerBundle\Entity\User;

/**
 * Class UserSubscriber
 * @package BugTrackerBundle\EventListener
 */
class UserSubscriber implements EventSubscriber
{

    /**
     * @var UserPasswordEncoder
     */
    private $encoder;

    /**
     * UserSubscriber constructor.
     * @param UserPasswordEncoder $encoder
     */
    public function __construct(UserPasswordEncoder $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return ['prePersist', 'preUpdate'];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->encodePassword($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->encodePassword($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    private function encodePassword(LifecycleEventArgs $args)
    {
        if (($entity = $args->getEntity()) && ($entity instanceof User) && ($password = $entity->getPlainPassword())) {
            $encoded_password = $this->encoder->encodePassword($entity, $password);

            $entity->setPassword($encoded_password);
        }
    }

}