<?php

namespace BugTrackerBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use BugTrackerBundle\Entity\Issue;

/**
 * Class IssueSubscriber
 * @package BugTrackerBundle\EventListener
 */
class IssueSubscriber implements EventSubscriber
{

    /**
     * @var TokenStorageInterface
     */
    private $token_storage;

    /**
     * IssueSubscriber constructor.
     * @param TokenStorageInterface $storage
     */
    public function __construct(TokenStorageInterface $storage)
    {
        $this->token_storage = $storage;
    }


    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return ['prePersist'];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        if (($entity = $args->getEntity()) && ($entity instanceof Issue) && ($token = $this->token_storage->getToken()) && ($user = $token->getUser())) {
            $entity->setCreatedBy($user);
        }
    }

}