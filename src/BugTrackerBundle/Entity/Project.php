<?php

namespace BugTrackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Project
 * @package BugTrackerBundle\Entity
 * @ORM\Entity(repositoryClass="BugTrackerBundle\Repository\ProjectRepository")
 * @ORM\Table(name="projects")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("slug")
 */
class Project
{

    use Traits\Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="is_active", type="boolean", length=1, options={"default" : 0})
     */
    private $is_active;

    /**
     * @ORM\ManyToMany(targetEntity="BugTrackerBundle\Entity\Tracker", inversedBy="projects")
     * @ORM\JoinTable(name="project_trackers")
     * @Assert\Count(min = 1)
     */
    private $trackers;

    /**
     * @ORM\ManyToMany(targetEntity="BugTrackerBundle\Entity\Priority", inversedBy="projects")
     * @ORM\JoinTable(name="project_priorities")
     * @Assert\Count(min = 1)
     */
    private $priorities;

    /**
     * @ORM\ManyToMany(targetEntity="BugTrackerBundle\Entity\User")
     * @ORM\JoinTable(
     *     name="project_members",
     *     joinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $members;

    public function __construct()
    {
        $this->trackers = new ArrayCollection();
        $this->priorities = new ArrayCollection();
        $this->members = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function beforeCreate()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function beforeUpdate()
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param mixed $members
     */
    public function setMembers($members)
    {
        $this->members = $members;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * @param mixed $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    /**
     * @return mixed
     */
    public function getTrackers()
    {
        return $this->trackers;
    }

    /**
     * @param mixed $trackers
     */
    public function setTrackers($trackers)
    {
        $this->trackers = $trackers;
    }

    /**
     * @return mixed
     */
    public function getPriorities()
    {
        return $this->priorities;
    }

    /**
     * @param mixed $priorities
     */
    public function setPriorities($priorities)
    {
        $this->priorities = $priorities;
    }

    public function addTracker($item = null)
    {
        if (!$this->trackers->contains($item)) {
            $this->trackers->add($item);
        }
    }

    public function removeTracker($item)
    {
        if ($this->trackers->contains($item)) {
            $this->trackers->removeElement(item);
        }
    }

    public function addPriority($item)
    {
        if (!$this->priorities->contains($item)) {
            $this->priorities->add($item);
        }
    }

    public function removePriority($item)
    {
        if ($this->priorities->contains($item)) {
            $this->priorities->removeElement(item);
        }
    }

    public function addMember($item)
    {
        if (!$this->members->contains($item)) {
            $this->members->add($item);
        }
    }

    public function removeMember($item)
    {
        if ($this->members->contains($item)) {
            $this->members->removeElement(item);
        }
    }

}