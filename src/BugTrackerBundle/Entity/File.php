<?php

namespace BugTrackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class File
 * @package BugTrackerBundle\Entity
 * @ORM\Entity(repositoryClass="BugTrackerBundle\Repository\FileRepository")
 * @ORM\Table(name="files")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="foreign_model", type="string")
 * @ORM\DiscriminatorMap({
 *     "File" = "File",
 *     "Issue" = "BugTrackerBundle\Entity\Attachment\IssueAttachment",
 *     "Project" = "BugTrackerBundle\Entity\Attachment\ProjectAttachment"
 * })
 */
class File
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $path;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }



}