<?php

namespace BugTrackerBundle\Entity\Attachment;

use Doctrine\ORM\Mapping as ORM;
use BugTrackerBundle\Entity\File;

/**
 * Class ProjectAttachment
 * @package BugTrackerBundle\Entity\Attachment
 * @ORM\Entity()
 * @ORM\Table(name="project_attachments")
 */
class ProjectAttachment extends File
{

}