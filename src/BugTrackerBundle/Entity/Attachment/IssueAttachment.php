<?php

namespace BugTrackerBundle\Entity\Attachment;

use Doctrine\ORM\Mapping as ORM;
use BugTrackerBundle\Entity\File;

/**
 * Class IssueAttachment
 * @package BugTrackerBundle\Entity\Attachment
 * @ORM\Entity()
 * @ORM\Table(name="issue_attachments")
 */
class IssueAttachment extends File
{

    /**
     * @ORM\ManyToOne(targetEntity="BugTrackerBundle\Entity\Issue", inversedBy="attachments")
     * @ORM\JoinColumn(name="issue_id")
     */
    private $issue;

    /**
     * @return mixed
     */
    public function getIssue()
    {
        return $this->issue;
    }

    /**
     * @param mixed $issue
     */
    public function setIssue($issue)
    {
        $this->issue = $issue;
    }



}