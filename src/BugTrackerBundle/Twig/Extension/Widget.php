<?php

namespace BugTrackerBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Widget extends \Twig_Extension
{

    /**
     * Normaly, injecting the whole container into a service/extension
     * is considered a bad practice.
     * In this case, we need to access a runtime assigned parameter "widgets"
     * as well as any number of internal service in case a certain widget
     * needs a reference.
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private static $collection = [];

    private static $instances = [];

    /**
     * Widget constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $widgets_collection = $container->getParameter('widgets');

        if (static::$collection || count(static::$collection) !== count($widgets_collection)) {
            static::$collection = $widgets_collection;
        }
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            'render_widget' => new \Twig_SimpleFunction('render_widget', [$this, 'renderWidget'], ['is_safe' => ['html']])
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'twig_widget_extension';
    }

    /**
     * @param $widget_id
     * @param array $params
     * @return string
     */
    public function renderWidget($widget_id, array $params = [])
    {
        if (array_key_exists($widget_id, static::$collection)) {
            $widget = static::$collection[$widget_id];
            $templating = $this->container->get('templating');
            $hash = md5($widget['class_name']);

            if (!isset(static::$instances[$hash])) {
                $object = new $widget['class_name']();

                if (!$object instanceof \BugTrackerBundle\Widget\AbstractWidget) {
                    throw new \RuntimeException(sprintf(
                        'Widget %s <%s> must extends BugTrackerBundle\\Widget\\AbstractWidget', $widget['class_name'], $widget_id
                    ));
                }

                $object->setTemplating($templating);
                if (isset($widget['calls']) && count($widget['calls'])) {
                    foreach ($widget['calls'] as $method => $param) {
                        if (strpos($param, '@') == 0) {
                            $service_id = str_replace('@', '', $param);
                            if ($this->container->has($service_id)) {
                                $param = $this->container->get($service_id);
                            } else {
                                throw new \InvalidArgumentException(sprintf(
                                    'You have requested non-existing service %s', $param
                                ));
                            }
                        }

                        call_user_func_array([$object, $method], [$param]);
                    }
                }

                static::$instances[$hash] = $object;
            }

            $instance = static::$instances[$hash];
            $instance->initialize($params);

            return $instance->getContent();
        }

        return '';
    }

}