<?php

namespace BugTrackerBundle\Twig\Extension;

use Symfony\Component\Routing\RouterInterface;

/**
 * Class HtmlHelper
 * @package BugTrackerBundle\Twig\Extension
 */
class HtmlHelper extends \Twig_Extension
{

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * HtmlHelper constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            'link_to' => new \Twig_SimpleFunction('link_to', [$this, 'linkTo'], ['is_safe' => ['html']]),
            'time_ago' => new \Twig_SimpleFunction('time_ago', [$this, 'timeAgo'], ['is_safe' => ['html']])
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'twig_html_helper_extension';
    }

    /**
     * @param string $title
     * @param $route
     * @param array $params
     * @param null $class
     * @return string
     */
    public function linkTo($title, $route, array $params = [], $class = null)
    {
        return sprintf(
            '<a href="%s" class="%s">%s</a>',
            $this->router->generate($route, $params),
            $class,
            $title
        );
    }

    /**
     * @param \DateTime $datetime
     * @return string
     */
    public function timeAgo(\DateTime $datetime = null)
    {
        if (is_null($datetime)) {
            return;
        }

        $timestamp = $datetime->getTimestamp();
        $estimated_time = time() - $timestamp;

        if ($estimated_time < 1) {
            return 'less than a second ago!';
        } elseif ($estimated_time > (2 * 24 * 60 * 60)) {
            return $datetime->format('d F Y H:i');
        }

        // Maybe should inject Translator later on...
        $map = [
            24 * 60 * 60            => 'day',
            60 * 60                 => 'hour',
            60                      => 'minute',
            1                       => 'second'
        ];

        foreach($map as $secs => $str) {
            $time = $estimated_time / $secs;

            if ($time >= 1) {
                $rounded_time = round($time);

                return sprintf(
                    'about %s %s ago',
                    $rounded_time,
                    $str . (($rounded_time > 1) ? 's' : '')
                );
            }
        }
    }

}