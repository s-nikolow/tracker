<?php

namespace BugTrackerBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class BugTrackerExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $application['widgets'] = [];
        if (isset($config['widgets']) && count($config['widgets'])) {
            foreach ($config['widgets'] as $key => $params) {
                if (isset($params['services']) && count($params['services'])) {
                    foreach ($params['services'] as $widget_id => $widget) {
                        $class_name = sprintf('%s\\%s', $params['namespace'], $widget['class']);
                        $collection['widgets'][$widget_id] = [
                            'class_name' => $class_name,
                            'calls' => isset($widget['calls']) ? $widget['calls'] : [],
                            'parameters' => isset($widget['parameters']) ? $widget['parameters'] : [],
                        ];
                    }
                }
            }
        }

        $container->setParameter('widgets', $collection['widgets']);
    }

}