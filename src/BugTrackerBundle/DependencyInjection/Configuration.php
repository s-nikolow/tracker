<?php

namespace BugTrackerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();
        $root = $builder->root('bug_tracker');

        $root
            ->children()
                ->arrayNode('widgets')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('namespace')->isRequired()->end()
                            ->arrayNode('services')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('class')->isRequired()->end()
                                        ->variableNode('calls')->end()
                                        ->variableNode('parameters')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end();

        return $builder;
    }

}