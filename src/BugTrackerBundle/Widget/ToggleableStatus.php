<?php

namespace BugTrackerBundle\Widget;

use Symfony\Component\Routing\RouterInterface;

/**
 * Class ToggableStatus
 * @package BugTrackerBundle\Widget
 */
class ToggleableStatus extends AbstractWidget
{

    /**
     * @var RouterInterface
     */
    private $router;

    public function setRouter(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        if (count(array_intersect_key($this->params, array_flip(['id', 'value', 'column', 'entity']))) !== 4) {
            return '';
        }

        $id = intval($this->params['id']);
        $value = $this->params['value'];
        $entity = $this->params['entity'];
        $column = $this->params['column'];

        $config = [
            ['class' => 'text-danger glyphicon glyphicon-remove'],
            ['class' => 'text-success glyphicon glyphicon-ok']
        ];

        return sprintf(
            '<a href="javascript:;" class="%s" data-class-list="text-danger glyphicon glyphicon-remove text-success glyphicon glyphicon-ok" data-url="%s" data-id="%d" data-entity="%s" data-column="%s" data-value="%d" data-role="togglable"></a>',
            $config[$value]['class'],
            $this->router->generate('ajax_set_opposite_value'),
            $id,
            $entity,
            $column,
            $value
        );
    }

}