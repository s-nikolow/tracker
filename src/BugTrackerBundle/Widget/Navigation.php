<?php

namespace BugTrackerBundle\Widget;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class Navigation
 * @package BugTrackerBundle\Widget
 */
class Navigation extends AbstractWidget
{

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var TokenStorageInterface
     */
    private $token_storage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $auth_checker;

    /**
     * @param RouterInterface $router
     */
    public function setRouter(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param TokenStorageInterface $storage
     */
    public function setTokenStorage(TokenStorageInterface $storage)
    {
        $this->token_storage = $storage;
    }

    /**
     * @param AuthorizationCheckerInterface $checker
     */
    public function setAuthChecker(AuthorizationCheckerInterface $checker)
    {
        $this->auth_checker = $checker;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        $collection = [
            'left' => [
                ['label' => $this->translator->trans('NAVIGATION.home'), 'link' => $this->router->generate('homepage')],
                ['label' => $this->translator->trans('NAVIGATION.my_projects'), 'link' => $this->router->generate('current_projects_list')],
                ['label' => $this->translator->trans('NAVIGATION.admin'), 'children' => [
                    ['label' => $this->translator->trans('NAVIGATION.issue_status'), 'link' => $this->router->generate('status_index')],
                    ['label' => $this->translator->trans('NAVIGATION.issue_tracker'), 'link' => $this->router->generate('tracker_index')],
                    ['label' => $this->translator->trans('NAVIGATION.issue_priority'), 'link' => $this->router->generate('priority_index')],
                    ['label' => $this->translator->trans('NAVIGATION.projects'), 'link' => $this->router->generate('project_index')],
                    ['label' => $this->translator->trans('NAVIGATION.users'), 'link' => $this->router->generate('user_index')]
                ]]
            ],
            'right' => [
                ['label' => $this->token_storage->getToken()->getUser()->getUsername(), 'children' => [
                    ['label' => $this->translator->trans('NAVIGATION.profile'), 'link' => $this->router->generate('profile_index')],
                    ['label' => $this->translator->trans('NAVIGATION.logout'), 'link' => $this->router->generate('profile_logout')]
                ]]
            ]
        ];

        return $this->renderTemplate('widgets/navigation.html.twig', [
            'collection' => $collection
        ]);
    }

}