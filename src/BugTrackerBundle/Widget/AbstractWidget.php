<?php

namespace BugTrackerBundle\Widget;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

/**
 * Class AbstractWidget
 * @package BugTrackerBundle\Widget
 */
abstract class AbstractWidget
{

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var array
     */
    public $params = [];

    /**
     * @param EngineInterface $templating
     */
    public function setTemplating(EngineInterface $templating)
    {
        $this->templating = $templating;
    }

    /**
     * @param array $params
     */
    public function initialize(array $params = [])
    {
        $this->params = $params;
        $this->beforeLoad();
    }

    protected function beforeLoad()
    {
    }

    /**
     * Every widget that extends this abstract class must define a getContent function.
     */
    abstract public function getContent();

    /**
     * @param string $template
     * @param array  $params
     */
    final public function renderTemplate($template, array $params = [])
    {
        return $this->templating->render($template, $params);
    }
}