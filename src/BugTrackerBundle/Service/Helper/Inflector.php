<?php

namespace BugTrackerBundle\Service\Helper;

class Inflector
{

    private static $cache = [
        'camelize' => [],
        'underscore' => [],
        'latinize' => [],
        'unaccent' => [],
        'urlize' => [],
        'slugalize' => []
    ];

    /**
     * @param string $word
     * @return mixed
     */
    public static function camelize($word)
    {
        if (isset(self::$cache['camelize'][$word])) {
            return self::$cache['camelize'][$word];
        }

        if (preg_match_all('/\/(.?)/',$word,$got)) {
            foreach ($got[1] as $k=>$v){
                $got[1][$k] = '::'.strtoupper($v);
            }

            $word = str_replace($got[0],$got[1],$word);
        }

        return self::$cache['camelize'][$word] = str_replace(' ', '', ucwords(preg_replace('/[^A-Z^a-z^0-9^:]+/', ' ', $word)));
    }

    /**
     * @param string $word
     * @return string
     */
    public static function underscore($word)
    {
        if (isset(self::$cache['underscore'][$word])) {
            return self::$cache['underscore'][$word];
        }

        $result = strtolower(preg_replace('/[^A-Z^a-z^0-9^\/]+/','_',
            preg_replace('/([a-z\d])([A-Z])/','\1_\2',
                preg_replace('/([A-Z]+)([A-Z][a-z])/','\1_\2',
                    preg_replace('/::/', '/', $word)
                )
            )
        ));

        return self::$cache['underscore'][$word] = $result;
    }

    /**
     * @param string $text
     * @return mixed
     */
    public static function latinize($text)
    {
        if (isset(self::$cache['latinize'][$text])) {
            return self::$cache['latinize'][$text];
        }

        $cyrillic = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ь', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ь', 'ю', 'я');
        $latin = array('A', 'B', 'V', 'G', 'D', 'E', 'J', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sht', 'Y', 'I', 'U', 'Ja', 'a', 'b', 'v', 'g', 'd', 'e', 'j', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sht', 'y', 'i', 'u', 'ja');

        return self::$cache['latinize'][$text] = str_replace($cyrillic, $latin, $text);
    }

    /**
     * @param string $text
     * @return mixed
     */
    public static function unaccent($text)
    {
        if (isset(self::$cache['unaccent'][$text])) {
            return self::$cache['unaccent'][$text];
        }

        $map = array(
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C',
            'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I',
            'Ð'=>'D', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O',
            'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'T', 'ß'=>'s', 'à'=>'a',
            'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e',
            'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'e',
            'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
            'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'þ'=>'t', 'ÿ'=>'y'
        );
        return self::$cache['unaccent'][$text] = str_replace(array_keys($map), array_values($map), $text);
    }

    /**
     * @param string $text
     * @return string
     */
    public static function urlize($text)
    {
        if (isset(self::$cache['urlize'][$text])) {
            return self::$cache['urlize'][$text];
        }

        return self::$cache['urlize'][$text] = trim(static::underscore(static::unaccent($text)), '_');
    }

    /**
     * @param string $text
     * @param string $delimiter
     * @return mixed
     */
    public static function slugalize($text, $delimiter = '-')
    {
        if (isset(self::$cache['slugalize'][$delimiter][$text])) {
            return self::$cache['slugalize'][$delimiter][$text];
        }

        return self::$cache['slugalize'][$delimiter][$text] = str_replace('_', $delimiter, static::urlize(str_replace('/', '_', static::latinize($text))));
    }

}