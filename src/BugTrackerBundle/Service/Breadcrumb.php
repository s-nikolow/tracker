<?php

namespace BugTrackerBundle\Service;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class Breadcrumb
 * @package BugTrackerBundle\Service
 */
class Breadcrumb
{

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var array
     */
    private $collection = [];

    /**
     * Breadcrumb constructor.
     * @param RouterInterface $router
     * @param TranslatorInterface $translator
     */
    public function __construct(RouterInterface $router, TranslatorInterface $translator)
    {
        $this->router = $router;
        $this->translator = $translator;

        $this->collection[] = ['label' => $translator->trans('BREADCRUMB.home'), 'link' => $router->generate('homepage')];
    }

    /**
     * @return array
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param $label
     * @param null $link
     * @param array $route_params
     * @param bool $translatable
     * @return Breadcrumb
     */
    public function append($label, $route = null, array $route_params = [], $translatable = true)
    {
        $label = $translatable ? $this->translator->trans($label) : $label;
        $link = !empty($route) ? $this->router->generate($route, $route_params) : null;

        $this->collection[] = ['label' => $label, 'link' => $link];

        return $this;
    }

}