<?php

namespace BugTrackerBundle\Enum;

class StatusEnum
{

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

}