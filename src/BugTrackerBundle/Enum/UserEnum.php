<?php

namespace BugTrackerBundle\Enum;

class UserEnum
{

    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_ROOT = 'ROLE_ROOT';

    /**
     * @return array
     */
    public static function getRolesToArray()
    {
        return [
            self::ROLE_USER => self::ROLE_USER,
            self::ROLE_ADMIN => self::ROLE_ADMIN,
            self::ROLE_ROOT => self::ROLE_ROOT
        ];
    }

}