<?php

namespace BugTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use BugTrackerBundle\Entity\User;
use BugTrackerBundle\Enum\UserEnum;

class UserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'label' => 'USERS.email',
            'translation_domain' => 'forms'
        ]);

        if (!empty($options['add_password']) && $options['add_password']) {
            $builder->add('plain_password', PasswordType::class, [
                'label' => 'USERS.password',
                'translation_domain' => 'forms'
            ]);
        }

        $builder->add('roles', ChoiceType::class, [
            'multiple' => true,
            'expanded' => true,
            'choices' => UserEnum::getRolesToArray(),
            'label' => 'USERS.roles',
            'translation_domain' => 'forms'
        ]);
        $builder->add('username', TextType::class, [
            'label' => 'USERS.username',
            'translation_domain' => 'forms'
        ]);
        $builder->add('is_active', CheckboxType::class, [
            'required' => false,
            'label' => 'USERS.is_active',
            'translation_domain' => 'forms'
        ]);
        $builder->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-sm btn-primary'],
            'label' => 'COMMON.BUTTONS.submit',
            'translation_domain' => 'forms'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'add_password' => false
        ]);
    }

}