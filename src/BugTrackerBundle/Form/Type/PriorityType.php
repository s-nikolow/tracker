<?php

namespace BugTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use BugTrackerBundle\Entity\Priority;

class PriorityType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'PRIORITY.title',
            'translation_domain' => 'forms'
        ]);
        $builder->add('class_name', TextType::class, [
            'label' => 'PRIORITY.class_name',
            'translation_domain' => 'forms',
            'required' => false
        ]);
        $builder->add('is_active', CheckboxType::class, [
            'required' => false,
            'label' => 'PRIORITY.is_active',
            'translation_domain' => 'forms'
        ]);
        $builder->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-sm btn-primary'],
            'label' => 'COMMON.BUTTONS.submit',
            'translation_domain' => 'forms'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Priority::class
        ]);
    }

}