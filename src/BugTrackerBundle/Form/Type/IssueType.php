<?php

namespace BugTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use BugTrackerBundle\Entity\Issue;
use BugTrackerBundle\Form\Type\Attachment\IssueAttachmentType;

class IssueType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $project = $options['project'];

        $builder->add('title', TextType::class, [
            'label' => 'ISSUE.title',
            'translation_domain' => 'forms'
        ]);
        $builder->add('tracker', EntityType::class, [
            'class' => 'BugTrackerBundle:Tracker',
            'choice_label' => 'title',
            'placeholder' => 'empty_value',
            'required' => false,
            'label' => 'ISSUE.tracker',
            'translation_domain' => 'forms',
            'query_builder' => function(EntityRepository $repo) use ($project) {
                $builder = $repo->createQueryBuilder('tracker');

                return $builder
                    ->innerJoin('tracker.projects', 'project')
                    ->where($builder->expr()->eq('project.id', ':project_id'))
                    ->setParameter('project_id', $project->getId());
            }
        ]);
        $builder->add('priority', EntityType::class, [
            'class' => 'BugTrackerBundle:Priority',
            'choice_label' => 'title',
            'placeholder' => 'empty_value',
            'required' => false,
            'label' => 'ISSUE.priority',
            'translation_domain' => 'forms',
            'query_builder' => function(EntityRepository $repo) use ($project) {
                $builder = $repo->createQueryBuilder('priority');

                return $builder
                    ->innerJoin('priority.projects', 'project')
                    ->where($builder->expr()->eq('project.id', ':project_id'))
                    ->setParameter('project_id', $project->getId());
            }
        ]);
        $builder->add('status', EntityType::class, [
            'class' => 'BugTrackerBundle:Status',
            'choice_label' => 'title',
            'label' => 'ISSUE.status',
            'translation_domain' => 'forms'
        ]);
        $builder->add('due_date', DateType::class, [
            'label' => 'ISSUE.due_date',
            'translation_domain' => 'forms',
            'widget' => 'single_text',
            'html5' => false,
            'required' => false,
            'attr' => [
                'data-role' => 'bs-datepicker'
            ]
        ]);
        $builder->add('description', TextareaType::class, [
            'label' => 'ISSUE.description',
            'translation_domain' => 'forms',
            'attr' => [
                'data-role' => 'ckeditor'
            ]
        ]);
        $builder->add('attachments', CollectionType::class, [
            'entry_type' => IssueAttachmentType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'required' => false,
            'label' => 'Прикачени файлове'
        ]);
        $builder->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-sm btn-primary'],
            'label' => 'COMMON.BUTTONS.submit',
            'translation_domain' => 'forms'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Issue::class,
            'project' => null
        ]);
    }

}