<?php

namespace BugTrackerBundle\Form\Type\Attachment;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use BugTrackerBundle\Entity\Attachment\IssueAttachment;
use BugTrackerBundle\Form\Custom\FileManagerType;

class IssueAttachmentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class);
        $builder->add('path', FileManagerType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => IssueAttachment::class
        ]);
    }

}