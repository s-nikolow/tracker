<?php

namespace BugTrackerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use BugTrackerBundle\Entity\Project;

class ProjectType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'PROJECT.title',
            'translation_domain' => 'forms'
        ]);
        $builder->add('slug', TextType::class, [
            'label' => 'PROJECT.slug',
            'translation_domain' => 'forms',
            'attr' => [
                'data-sluggable' => 'true',
                'data-depending-on' => 'project[title]'
            ]
        ]);
        $builder->add('description', TextareaType::class, [
            'label' => 'PROJECT.description',
            'translation_domain' => 'forms',
            'attr' => [
                'data-role' => 'ckeditor'
            ]
        ]);
        $builder->add('trackers', EntityType::class, [
            'class' => 'BugTrackerBundle:Tracker',
            'choice_label' => 'title',
            'expanded' => true,
            'multiple' => true,
            'label' => false
        ]);
        $builder->add('priorities', EntityType::class, [
            'class' => 'BugTrackerBundle:Priority',
            'choice_label' => 'title',
            'expanded' => true,
            'multiple' => true,
            'label' => false
        ]);
        $builder->add('is_active', CheckboxType::class, [
            'required' => false,
            'label' => 'PROJECT.is_active',
            'translation_domain' => 'forms'
        ]);
        $builder->add('submit', SubmitType::class, [
            'attr' => ['class' => 'btn btn-sm btn-primary'],
            'label' => 'COMMON.BUTTONS.submit',
            'translation_domain' => 'forms'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class
        ]);
    }

}