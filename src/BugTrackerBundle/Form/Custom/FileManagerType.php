<?php

namespace BugTrackerBundle\Form\Custom;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FileManagerType extends AbstractType
{

    /**
     * @return mixed
     */
    public function getParent()
    {
        return TextType::class;
    }

}