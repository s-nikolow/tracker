<?php

namespace BugTrackerBundle\Command;

use BugTrackerBundle\Entity\Priority;
use BugTrackerBundle\Entity\Project;
use BugTrackerBundle\Entity\Status;
use BugTrackerBundle\Entity\Tracker;
use BugTrackerBundle\Entity\User;
use BugTrackerBundle\Enum\UserEnum;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class InitialDataCommand extends ContainerAwareCommand
{

    /** @var \Doctrine\ORM\EntityManager */
    private $entityManager;

    /** @var  ContainerInterface */
    private $container;

    /**
     * Register command.
     *
     * This command should be called after our schema is built.
     */
    protected function configure()
    {
        $this
            ->setName('seed:init')
            ->setDescription('Populate database with necessary data.');
    }

    /**
     * Execute command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getContainer();
        $this->entityManager = $this->container->get('doctrine')->getEntityManager();

        // Create and persist users
        $output->writeln('[0] Inserting users...');
        $this->addUser();

        // Create and persist priorities
        $output->writeln('[1] Inserting priorities...');
        $this->addPriorities();

        // Create and persist trackers
        $output->writeln('[2] Inserting trackers...');
        $this->addTrackers();

        // Create and persist issue statuses
        $output->writeln('[3] Inserting statuses...');
        $this->addStatuses();

        // Create and persist project
        $output->writeln('[4] Inserting project...');
        $this->addProject();

        // Flush everything and keep on dancing!
        $this->entityManager->flush();
        $output->writeln('Done...');
    }

    /**
     * Add demo user with admin privilegies
     */
    public function addUser()
    {
        $entity = new User();
        $password = $this->container->get('security.encoder_factory')->getEncoder($entity)->encodePassword('admin', $entity->getSalt());

        $entity->setEmail('root@devzone.eu');
        $entity->setRoles([UserEnum::ROLE_USER, UserEnum::ROLE_ADMIN, UserEnum::ROLE_ROOT]);
        $entity->setIsActive(true);
        $entity->setUsername('Станислав Николов');
        $entity->setPassword($password);

        $this->entityManager->persist($entity);
    }

    /**
     * Add data to priorities table.
     *
     * @return void
     */
    private function addPriorities()
    {
        $data = array(
            'Low',
            'Normal',
            'High',
            'Urgent',
            'Immediate',
        );

        foreach ($data as $priority) {
            $entity = new Priority();
            $entity->setTitle($priority);

            // Since class names are defined like
            // priority-low or priority-normal
            // we can simply use the title as base to format our class name
            $className = sprintf('priority-%s', strtolower($priority));
            $entity->setClassName($className);
            $entity->setIsActive(true);

            $this->entityManager->persist($entity);
        }
    }

    /**
     * Add data to trackers table
     *
     * @return void
     */
    private function addTrackers()
    {
        $data = array(
            'Bug',
            'Feature',
            'Support',
        );

        foreach ($data as $tracker) {
            $entity = new Tracker();
            $entity->setTitle($tracker);
            $entity->setIsActive(true);

            $this->entityManager->persist($entity);
        }
    }

    /**
     * Add data to issue_statuses table
     *
     * @return void
     */
    private function addStatuses()
    {
        $data = array(
            'New',
            'In Progress',
            'Resolved',
            'Feedback',
            'Closed',
            'Rejected',
        );

        foreach ($data as $key => $status) {
            $entity = new Status();
            $entity->setTitle($status);

            // We can use the same class name formatting as above
            // Only one of the classes needs specific formatting tho.
            $class = ($key === 1) ? 'progress' : $status;
            $className = sprintf('status-%s', strtolower($class));
            $entity->setClassName($className);
            $entity->setIsActive(true);

            $this->entityManager->persist($entity);
        }
    }

    /**
     * Create new project
     *
     * @return void
     */
    private function addProject()
    {
        $entity = new Project();

        $entity->addTracker($this->entityManager->getReference('BugTrackerBundle:Tracker', 1));
        $entity->addTracker($this->entityManager->getReference('BugTrackerBundle:Tracker', 2));

        $entity->addPriority($this->entityManager->getReference('BugTrackerBundle:Priority', 1));
        $entity->addPriority($this->entityManager->getReference('BugTrackerBundle:Priority', 2));
        $entity->addPriority($this->entityManager->getReference('BugTrackerBundle:Priority', 3));
        $entity->addPriority($this->entityManager->getReference('BugTrackerBundle:Priority', 4));
        $entity->addPriority($this->entityManager->getReference('BugTrackerBundle:Priority', 5));

        $entity->addMember($this->entityManager->getReference('BugTrackerBundle:User', 1));

        $entity->setTitle('Примерен проект');
        $entity->setSlug('primeren-proekt');
        $entity->setDescription('Lorem Ipsum е елементарен примерен текст, използван в печатарската и типографската индустрия. Lorem Ipsum е индустриален стандарт от около 1500 година, когато неизвестен печатар взема няколко печатарски букви и ги разбърква, за да напечата с тях книга с примерни шрифтове. Този начин не само е оцелял повече от 5 века, но е навлязъл и в публикуването на електронни издания като е запазен почти без промяна. Популяризиран е през 60те години на 20ти век със издаването на Letraset листи, съдържащи Lorem Ipsum пасажи, популярен е и в наши дни във софтуер за печатни издания като Aldus PageMaker, който включва различни версии на Lorem Ipsum.');
        $entity->setIsActive(true);

        $this->entityManager->persist($entity);
    }

}