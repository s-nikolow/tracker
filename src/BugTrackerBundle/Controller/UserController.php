<?php

namespace BugTrackerBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use BugTrackerBundle\Entity\User;
use BugTrackerBundle\Repository\UserRepository;
use BugTrackerBundle\Form\Type\UserType;

/**
 * Class UserController
 * @package BugTrackerBundle\Controller
 * @Route("/modules/user")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends AbstractBaseController
{

    /**
     * @return string
     * @Route("/", name="user_index")
     */
    public function indexAction()
    {
        $this->setLayout('crud');
        $this->get('breadcrumb')
            ->append('BREADCRUMB.user', 'user_index');

        return $this->render('user/index.html.twig', [
            'collection' => $this->getRepository('BugTrackerBundle:User')->findCollectionForList($cache = true)
        ]);
    }

    /**
     * @param Request $request
     * @return string
     * @Route("/create", name="user_create")
     */
    public function createAction(Request $request)
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.user', 'user_index')
            ->append('BREADCRUMB.common.add');

        $entity = new User();
        $form = $this->createForm(UserType::class, $entity, ['add_password' => true]);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param User $entity
     * @return string
     * @Route("/update/{id}", name="user_update", requirements={"id"="\d+"})
     */
    public function updateAction(Request $request, User $entity)
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.user', 'user_index')
            ->append('BREADCRUMB.common.edit');

        $form = $this->createForm(UserType::class, $entity);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param User $entity
     * @Route("/delete/{id}", name="user_delete", requirements={"id"="\d+"})
     */
    public function deleteAction(User $entity)
    {

    }

}