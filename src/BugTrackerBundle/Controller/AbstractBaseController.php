<?php

namespace BugTrackerBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class AbstractBaseController
 * @package BugTrackerBundle\Controller
 */
abstract class AbstractBaseController extends Controller
{

    /**
     * @var string
     */
    protected $layout = 'horizontal';

    /**
     * @var string
     */
    protected $default_layout = 'horizontal';

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param string $entity
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository($entity)
    {
        return $this->getEntityManager()->getRepository($entity);
    }

    /**
     * @param mixed $entity
     * @return void
     */
    public function persistAndFlush($entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    /**
     * @param string $view
     * @param array $parameters
     * @param Response|null $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($view, array $parameters = array(), Response $response = null)
    {
        if (!isset($parameters['layout'])) {
            $parameters['layout'] = $this->getLayoutPath($this->layout);
        }

        $parameters['default_layout'] = $this->getLayoutPath($this->default_layout);
        $parameters['breadcrumb_items'] = $this->get('breadcrumb')->getCollection();

        return parent::render($view, $parameters, $response);
    }

    /**
     * @param string $layout
     * @return void
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return string
     */
    private function getLayoutPath($layout)
    {
        return sprintf('layouts/%s.html.twig', $layout);
    }

}