<?php

namespace BugTrackerBundle\Controller\XHR;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Finder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityRepository;

class ActionController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/set-opposite-value", name="ajax_set_opposite_value")
     */
    public function setOppositeValue(Request $request)
    {
        $response = ['success' => false];

        if ($request->isXmlHttpRequest()) {
            $attributes = $request->request->all();

            if (count(array_intersect_key($attributes, array_flip(['column', 'entity', 'id', 'value']))) === 4) {
                $repository = $this->get('doctrine')->getManager()->getRepository($attributes['entity']);

                if ($repository instanceof EntityRepository) {
                    $result = $repository->setOppositeValue($attributes['column'], $attributes['value'], $attributes['id']);

                    $response['success'] = boolval($result);
                    $response['value'] = intval(!$attributes['value']);
                }
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/file-manager", name="ajax_file_manager")
     */
    public function initFileManagerTree(Request $request)
    {
        $response = [];

        if ($path = $request->request->get('path')) {
            $root_path = rtrim(dirname($this->getParameter('kernel.root_dir')), '/');
            $request_path = sprintf('%s/web/files/%s', $root_path, ltrim(filter_var($path, FILTER_SANITIZE_STRING), '/'));

            if (is_dir($request_path)) {
                $finder = (new Finder())->depth('== 0')->in($request_path)->sortByType();

                foreach ($finder as $item) {
                    $object = new \stdClass();

                    $object->name = $item->getFilename();
                    $object->type = is_dir($item->getRealPath()) ? 'folder' : 'file';

                    $response[$path][] = $object;
                }
            }
        }

        return new JsonResponse($response);
    }

}