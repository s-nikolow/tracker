<?php

namespace BugTrackerBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use BugTrackerBundle\Entity\Status;
use BugTrackerBundle\Repository\StatusRepository;
use BugTrackerBundle\Form\Type\StatusType;

/**
 * Class StatusController
 * @package BugTrackerBundle\Controller
 * @Route("/modules/status")
 * @Security("has_role('ROLE_ADMIN')")
 */
class StatusController extends AbstractBaseController
{

    /**
     * @return string
     * @Route("/", name="status_index")
     */
    public function indexAction()
    {
        $this->setLayout('crud');
        $this->get('breadcrumb')
            ->append('BREADCRUMB.status', 'status_index');

        return $this->render('status/index.html.twig', [
            'collection' => $this->getRepository('BugTrackerBundle:Status')->findCollectionForList($cache = true)
        ]);
    }

    /**
     * @param Request $request
     * @return string
     * @Route("/create", name="status_create")
     */
    public function createAction(Request $request)
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.status', 'status_index')
            ->append('BREADCRUMB.common.add');

        $entity = new Status();
        $form = $this->createForm(StatusType::class, $entity);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('status_index');
        }

        return $this->render('status/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Status $entity
     * @return string
     * @Route("/update/{id}", name="status_update", requirements={"id"="\d+"})
     */
    public function updateAction(Request $request, Status $entity)
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.status', 'status_index')
            ->append('BREADCRUMB.common.edit');

        $form = $this->createForm(StatusType::class, $entity);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('status_index');
        }

        return $this->render('status/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Status $entity
     * @Route("/delete/{id}", name="status_delete", requirements={"id"="\d+"})
     */
    public function deleteAction(Status $entity)
    {

    }

}