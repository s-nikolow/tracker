<?php

namespace BugTrackerBundle\Controller;

use BugTrackerBundle\BugTrackerBundle;
use BugTrackerBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoginController
 * @package BugTrackerBundle\Controller
 * @Route("/profile")
 */
class ProfileController extends AbstractBaseController
{

    protected $layout = 'login';

    /**
     * @Route("/", name="profile_index")
     */
    public function indexAction()
    {
        $this->setLayout('horizontal');

        return $this->render('profile/index.html.twig');
    }

    /**
     * @Route("/login", name="profile_login")
     */
    public function loginAction(Request $request)
    {
        $auth = $this->get('security.authentication_utils');

        return $this->render('profile/login.html.twig', [
            'error' => $auth->getLastAuthenticationError(),
            'email' => $auth->getLastUsername()
        ]);
    }

}