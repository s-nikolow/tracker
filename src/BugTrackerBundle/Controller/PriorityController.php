<?php

namespace BugTrackerBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use BugTrackerBundle\Entity\Priority;
use BugTrackerBundle\Repository\PriorityRepository;
use BugTrackerBundle\Form\Type\PriorityType;

/**
 * Class PriorityController
 * @package BugTrackerBundle\Controller
 * @Route("/modules/priority")
 * @Security("has_role('ROLE_ADMIN')")
 */
class PriorityController extends AbstractBaseController
{

    /**
     * @return string
     * @Route("/", name="priority_index")
     */
    public function indexAction()
    {
        $this->setLayout('crud');
        $this->get('breadcrumb')
            ->append('BREADCRUMB.priority', 'priority_index');

        return $this->render('priority/index.html.twig', [
            'collection' => $this->getRepository('BugTrackerBundle:Priority')->findCollectionForList($cache = true)
        ]);
    }

    /**
     * @param Request $request
     * @return string
     * @Route("/create", name="priority_create")
     */
    public function createAction(Request $request)
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.priority', 'priority_index')
            ->append('BREADCRUMB.common.add');

        $entity = new Priority();
        $form = $this->createForm(PriorityType::class, $entity);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('priority_index');
        }

        return $this->render('priority/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Priority $entity
     * @return string
     * @Route("/update/{id}", name="priority_update", requirements={"id"="\d+"})
     */
    public function updateAction(Request $request, Priority $entity)
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.priority', 'priority_index')
            ->append('BREADCRUMB.common.edit');

        $form = $this->createForm(PriorityType::class, $entity);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('priority_index');
        }

        return $this->render('priority/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Priority $entity
     * @Route("/delete/{id}", name="priority_delete", requirements={"id"="\d+"})
     */
    public function deleteAction(Priority $entity)
    {

    }

}