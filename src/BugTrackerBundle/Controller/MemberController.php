<?php

namespace BugTrackerBundle\Controller;

use BugTrackerBundle\Entity\Issue;
use BugTrackerBundle\Form\Type\IssueType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class MemberController
 * @package BugTrackerBundle\Controller
 */
class MemberController extends AbstractBaseController
{

    /**
     * @Route("/projects", name="current_projects_list")
     */
    public function currentProjectsListAction()
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.my_projects', 'current_projects_list');

        $collection = $this->getRepository('BugTrackerBundle:Project')->findMemberProjects($this->getUser(), $cache = true);

        return $this->render('member/list_projects.html.twig', [
            'collection' => $collection
        ]);
    }

    /**
     * @param string $slug
     * @Route("/projects/{slug}", name="browse_project")
     */
    public function browseProjectAction($slug)
    {
        if (!$project = $this->getRepository('BugTrackerBundle:Project')->findProject($slug, $this->getUser(), $cache = true)) {
            throw new AccessDeniedHttpException('Access Denied!');
        }

        $this->setLayout('crud');
        $this->get('breadcrumb')
            ->append('BREADCRUMB.my_projects', 'current_projects_list')
            ->append($project->getTitle(), 'browse_project', ['slug' => $project->getSlug()], $translatable = false)
            ->append('BREADCRUMB.issues');

        $collection = $this->getRepository('BugTrackerBundle:Issue')->findCollectionForProject($project, $cache = true);

        return $this->render('member/browse_project.html.twig', [
            'project' => $project,
            'collection' => $collection,
            'trackers' => $this->getRepository('BugTrackerBundle:Project')->findAssignedTrackerItems($project, $cache = true),
            'priorities' => $this->getRepository('BugTrackerBundle:Project')->findAssignedPriorityItems($project, $cache = true),
            'statuses' => $this->getRepository('BugTrackerBundle:Status')->findCollectionForProject()
        ]);
    }

    /**
     * @param Request $request
     * @param string $slug
     * @Route("/projects/{slug}/issue/create", name="issue_create")
     */
    public function createIssueAction(Request $request, $slug)
    {
        if (!$project = $this->getRepository('BugTrackerBundle:Project')->findProject($slug, $this->getUser(), $cache = true)) {
            throw new AccessDeniedHttpException('Access Denied!');
        }

        $this->setLayout('crud');
        $this->get('breadcrumb')
            ->append('BREADCRUMB.my_projects', 'current_projects_list')
            ->append($project->getTitle(), 'browse_project', ['slug' => $project->getSlug()], $translatable = false)
            ->append('BREADCRUMB.create_issue');

        $entity = new Issue();
        $entity->setProject($project);
        $form = $this->createForm(IssueType::class, $entity, ['project' => $project]);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('browse_issue', ['slug' => $project->getSlug(), 'issue_slug' => $entity->getSlug()]);
        }

        return $this->render('member/manage_issue.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Issue $entity
     * @param string $slug
     * @Route("/projects/{slug}/issue/update/{id}", name="issue_update")
     */
    public function updateIssueAction(Request $request, Issue $entity, $slug)
    {
        if (!$project = $this->getRepository('BugTrackerBundle:Project')->findProject($slug, $this->getUser(), $cache = true)) {
            throw new AccessDeniedHttpException('Access Denied!');
        }

        $this->setLayout('crud');
        $this->get('breadcrumb')
            ->append('BREADCRUMB.my_projects', 'current_projects_list')
            ->append($project->getTitle(), 'browse_project', ['slug' => $project->getSlug()], $translatable = false)
            ->append('BREADCRUMB.update_issue');

        $form = $this->createForm(IssueType::class, $entity, ['project' => $project]);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('browse_issue', ['slug' => $project->getSlug(), 'issue_slug' => $entity->getSlug()]);
        }

        return $this->render('member/manage_issue.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param string $slug
     * @param string $issue_slug
     * @Route("/projects/{slug}/issue/{issue_slug}", name="browse_issue")
     */
    public function viewIssueAction($slug, $issue_slug)
    {
        if (!$project = $this->getRepository('BugTrackerBundle:Project')->findProject($slug, $this->getUser(), $cache = true)) {
            throw new AccessDeniedHttpException('Access Denied!');
        }

        if (!$issue = $this->getRepository('BugTrackerBundle:Issue')->findSingleIssue($project, $issue_slug, $cache = true)) {
            throw new NotFoundHttpException('Resource not found!');
        }

        $this->get('breadcrumb')
            ->append('BREADCRUMB.my_projects', 'current_projects_list')
            ->append($project->getTitle(), 'browse_project', ['slug' => $project->getSlug()], $translatable = false)
            ->append($issue->getTitle(), null, [], $translatable = false);

        return $this->render('member/view_issue.html.twig', [
            'project' => $project,
            'issue' => $issue
        ]);
    }

}