<?php

namespace BugTrackerBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use BugTrackerBundle\Entity\Tracker;
use BugTrackerBundle\Repository\TrackerRepository;
use BugTrackerBundle\Form\Type\TrackerType;

/**
 * Class TrackerController
 * @package BugTrackerBundle\Controller
 * @Route("/modules/tracker")
 * @Security("has_role('ROLE_ADMIN')")
 */
class TrackerController extends AbstractBaseController
{

    /**
     * @return string
     * @Route("/", name="tracker_index")
     */
    public function indexAction()
    {
        $this->setLayout('crud');
        $this->get('breadcrumb')
            ->append('BREADCRUMB.tracker', 'tracker_index');

        return $this->render('tracker/index.html.twig', [
            'collection' => $this->getRepository('BugTrackerBundle:Tracker')->findCollectionForList($cache = true)
        ]);
    }

    /**
     * @param Request $request
     * @return string
     * @Route("/create", name="tracker_create")
     */
    public function createAction(Request $request)
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.tracker', 'tracker_index')
            ->append('BREADCRUMB.common.add');

        $entity = new Tracker();
        $form = $this->createForm(TrackerType::class, $entity);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('tracker_index');
        }

        return $this->render('tracker/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Tracker $entity
     * @return string
     * @Route("/update/{id}", name="tracker_update", requirements={"id"="\d+"})
     */
    public function updateAction(Request $request, Tracker $entity)
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.tracker', 'tracker_index')
            ->append('BREADCRUMB.common.edit');

        $form = $this->createForm(TrackerType::class, $entity);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('tracker_index');
        }

        return $this->render('tracker/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Tracker $entity
     * @Route("/delete/{id}", name="tracker_delete", requirements={"id"="\d+"})
     */
    public function deleteAction(Tracker $entity)
    {

    }

}