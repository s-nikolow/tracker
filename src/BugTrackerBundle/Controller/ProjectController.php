<?php

namespace BugTrackerBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use BugTrackerBundle\Entity\Project;
use BugTrackerBundle\Repository\ProjectRepository;
use BugTrackerBundle\Form\Type\ProjectType;

/**
 * Class ProjectController
 * @package BugTrackerBundle\Controller
 * @Route("/modules/project")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ProjectController extends AbstractBaseController
{

    /**
     * @return string
     * @Route("/", name="project_index")
     */
    public function indexAction()
    {
        $this->setLayout('crud');
        $this->get('breadcrumb')
            ->append('BREADCRUMB.project', 'project_index');

        return $this->render('project/index.html.twig', [
            'collection' => $this->getRepository('BugTrackerBundle:Project')->findCollectionForList($cache = true)
        ]);
    }

    /**
     * @param Request $request
     * @return string
     * @Route("/create", name="project_create")
     */
    public function createAction(Request $request)
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.project', 'project_index')
            ->append('BREADCRUMB.common.add');

        $entity = new Project();
        $form = $this->createForm(ProjectType::class, $entity);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('project_index');
        }

        return $this->render('project/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Project $entity
     * @return string
     * @Route("/update/{id}", name="project_update", requirements={"id"="\d+"})
     */
    public function updateAction(Request $request, Project $entity)
    {
        $this->get('breadcrumb')
            ->append('BREADCRUMB.project', 'project_index')
            ->append('BREADCRUMB.common.edit');

        $form = $this->createForm(ProjectType::class, $entity);

        if ($form->handleRequest($request) && ($form->isSubmitted() && $form->isValid())) {
            $this->persistAndFlush($entity);

            return $this->redirectToRoute('project_index');
        }

        return $this->render('project/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Project $entity
     * @Route("/delete/{id}", name="project_delete", requirements={"id"="\d+"})
     */
    public function deleteAction(Project $entity)
    {

    }

}