tracker
=======

A Symfony project created on March 18, 2017, 9:23 pm.

=======

## Quick start

    1. $ chmod 777 var/ -R && chmod 755 bin/console
    2. $ bin/console doctrine:schema:create
    3. $ bin/console seed:init

###Login credentials:

> email: root@devzone.eu

> password: admin

Re-compile **styles.scss** if needed.